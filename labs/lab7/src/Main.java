import java.util.Arrays;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Main {
    private static final int NAME = 0;
    private static final int DESG = 1;
    private static final int MAG = 2;
    private static ArrayList<Star> stars = new ArrayList();

    public static void main(String[] args) throws FileNotFoundException {
        // load star data
        Scanner file = new Scanner(new File("src/StarData.txt"));
        while (file.hasNextLine()) {
            // note: the pipe(|) needs to be escaped
            String[] lineData = file.nextLine().split("\\|");
            // initialize star from file data and save in an ArrayList container
            stars.add(new Star(lineData[NAME], lineData[DESG], Double.valueOf(lineData[MAG])));
        }

        // dump loaded star data
//        for (Star star : stars) System.out.println(star);

        // collide all stars, two at a time
        Star composite = stars.remove(0);
        while (stars.size() > 0) {
            composite = composite.collide(stars.remove(0));
        }

        // print out the last remaining star
        System.out.println(composite);
    }
}
