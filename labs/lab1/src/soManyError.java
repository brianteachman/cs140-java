/**
 * Brian Teachman
 * 9/26/2017
 * CS 140
 *
 * File: soManyErrors.java
 *
 * Lab 1: Fix all the errors in the given Java class file.
 */

public class soManyError {
    // Added return type (void) and changed String type to string(char?) array
    public static void main(String[] args){
        // added closing paren
        System.out.println("some example text");
        // added semicolon to end statement
        System.out.println("some other text");
        // print is a menber of out, so addedd
        System.out.print("YourName");
    }
}
