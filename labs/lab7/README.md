The goal of this lab will be to create a star class as a base for your own datatype Star. Another goal of the assignment is to work with this new datatype to create a program.

The class should contain fields for the parts of data you need to store(Star name, Stellar designation, Magnitude). In addition you should have a constructor for the objects of the class, a collide method, a getMagnitude method and a toString method.

1. public String toString() : Turns the current object into a string

2. public Star collide(Star bang) : combines the magnitudes of the new star(bang). The name and stellar designation will be of your choosing.

3. public double getMagnitude() : accessor to return the Magnitude of a star.

Once all of this is defined, read in the data from the StarData.txt data file and create Star objects from the data. 

Finally collide all of the stars, two at a time, and print out the final star that is left.


When you have finished the lab, upload your Star class and the file that creates and collides stars.
