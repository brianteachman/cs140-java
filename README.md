# CS 140: Computer Programming Fundamentals I

My working dir for CS140 class at Whatcom Communtiy College, based on the book Building Java Programs - A Back to Basics Approach (2E).

This code is product of my efforts in CS140, Computer Programming Fundamentals I class in the Fall of 2017.

### Topics

1. Data types and representation
2. Program control structures
3. Functions
4. File input/output
5. Crafting and debugging code

### Links

* [Java 8 API](http://docs.oracle.com/javase/8/docs/api/overview-summary.html)
* [http://www.buildingjavaprograms.com/supplements4.shtml](http://www.buildingjavaprograms.com/supplements4.shtml)
* [https://practiceit.cs.washington.edu/](https://practiceit.cs.washington.edu/)
