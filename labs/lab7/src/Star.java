public class Star {

    // static for unique object designations
    // starting at one then pre incrementing, includes this object in the count
    private static int starCount = 1;

    private String name;
    private String designation;
    private double magnitude;   // Magnitude of a star

    public Star(String starName, String stellarDesignation, double magnitude) {
        name = starName;
        designation = stellarDesignation;
        this.magnitude = magnitude;
    }

    public String toString() {
        return String.format("%s, %s, %.2f", name, designation, magnitude);
    }

    // Combines the magnitudes of the new star(bang)
    public Star collide(Star bang) {
        // The name and stellar designation will be of your choosing.
        String newName = name.substring(0, 3) + bang.toString().substring(0, 3);
        String newDesg = "NS-"+(++starCount); // NS => New Star
        double newMag = this.magnitude + bang.getMagnitude();
        return new Star(newName, newDesg, newMag);
    }

    public double getMagnitude() {
        return magnitude;
    }
}
